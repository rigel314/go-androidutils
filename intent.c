#include <string.h>

#include "_cgo_export.h"
#include "intent.h"

jstring NewStringUTF(JNIEnv *env, _GoString_ str)
{
	return (*env)->NewStringUTF(env, au_GoStringPtr(str));
}
int StringLength(JNIEnv *env, jstring str)
{
	return (int)(*env)->GetStringLength(env, str);
}
void GetStringUTF(JNIEnv *env, unsigned char *dest, jstring str, int len)
{
	const char *fromjstr = (*env)->GetStringUTFChars(env, str, NULL);
	memcpy(dest, fromjstr, len);
	(*env)->ReleaseStringUTFChars(env, str, fromjstr);
	return;
}

int ArrayLength(JNIEnv *env, jarray arr)
{
	return (int)(*env)->GetArrayLength(env, arr);
}
jobject ArrayIndex(JNIEnv *env, jarray arr, int idx)
{
	return (*env)->GetObjectArrayElement(env, arr, idx);
}

jclass FindClass(JNIEnv *env, _GoString_ name)
{
	return (*env)->FindClass(env, au_GoStringPtr(name));
}

jmethodID GetMethodID(JNIEnv *env, jclass clazz, _GoString_ name, _GoString_ sig)
{
	return (*env)->GetMethodID(env, clazz, au_GoStringPtr(name), au_GoStringPtr(sig));
}

jmethodID GetStaticMethodID(JNIEnv *env, jclass clazz, _GoString_ name, _GoString_ sig)
{
	return (*env)->GetStaticMethodID(env, clazz, au_GoStringPtr(name), au_GoStringPtr(sig));
}

jfieldID GetStaticFieldID(JNIEnv *env, jclass clazz, _GoString_ name, _GoString_ sig)
{
	return (*env)->GetStaticFieldID(env, clazz, au_GoStringPtr(name), au_GoStringPtr(sig));
}

jobject GetStaticObjectField(JNIEnv *env, jclass clazz, jfieldID fieldID)
{
	return (*env)->GetStaticObjectField(env, clazz, fieldID);
}

jobject NewObject_1arg_jstr(JNIEnv *env, jclass clazz, jmethodID methodID, jstring str)
{
	return (*env)->NewObject(env, clazz, methodID, str);
}
jobject NewObject_2arg_jstr(JNIEnv *env, jclass clazz, jmethodID methodID, jstring str1, jstring str2)
{
	return (*env)->NewObject(env, clazz, methodID, str1, str2);
}

jclass GetObjectClass(JNIEnv *env, jobject obj)
{
	return (*env)->GetObjectClass(env, obj);
}

void CallVoidMethod_1arg_jobj(JNIEnv *env, jobject obj, jmethodID methodID, jobject arg)
{
	return (*env)->CallVoidMethod(env, obj, methodID, arg);
}
jobject CallObjectMethod_0arg(JNIEnv *env, jobject obj, jmethodID methodID)
{
	return (*env)->CallObjectMethod(env, obj, methodID);
}
jobject CallObjectMethod_1arg_jobj(JNIEnv *env, jobject obj, jmethodID methodID, jobject arg)
{
	return (*env)->CallObjectMethod(env, obj, methodID, arg);
}
jobject CallObjectMethod_1arg_jstr(JNIEnv *env, jobject obj, jmethodID methodID, jstring arg)
{
	return (*env)->CallObjectMethod(env, obj, methodID, arg);
}

jobject CallStaticObjectMethod_0arg(JNIEnv *env, jclass clazz, jmethodID methodID)
{
	return (*env)->CallStaticObjectMethod(env, clazz, methodID);
}
jobject CallStaticObjectMethod_2arg_long_int(JNIEnv *env, jclass clazz, jmethodID methodID, long arg1, int arg2)
{
	return (*env)->CallStaticObjectMethod(env, clazz, methodID, arg1, arg2);
}
