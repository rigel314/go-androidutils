#ifndef INTENT_H_
#define INTENT_H_

#include <android/configuration.h>
#include <android/input.h>
#include <android/keycodes.h>
#include <android/looper.h>
#include <android/native_activity.h>
#include <android/native_window.h>
#include <EGL/egl.h>
#include <jni.h>
#include <pthread.h>
#include <stdlib.h>

// Assume _cgo_export.h is already included

size_t au_GoStringLen(_GoString_ s);
const char *au_GoStringPtr(_GoString_ s);

jstring NewStringUTF(JNIEnv *env, _GoString_ str);
int StringLength(JNIEnv *env, jstring str);
void GetStringUTF(JNIEnv *env, unsigned char *dest, jstring str, int len);
int ArrayLength(JNIEnv *env, jarray arr);
jobject ArrayIndex(JNIEnv *env, jarray arr, int idx);
jclass FindClass(JNIEnv *env, _GoString_ name);
jmethodID GetMethodID(JNIEnv *env, jclass clazz, _GoString_ name, _GoString_ sig);
jmethodID GetStaticMethodID(JNIEnv *env, jclass clazz, _GoString_ name, _GoString_ sig);
jfieldID GetStaticFieldID(JNIEnv *env, jclass clazz, _GoString_ name, _GoString_ sig);
jobject GetStaticObjectField(JNIEnv *env, jclass clazz, jfieldID fieldID);
jobject CallObjectMethod_0arg(JNIEnv *env, jobject obj, jmethodID methodID);
jobject NewObject_1arg_jstr(JNIEnv *env, jclass clazz, jmethodID methodID, jstring str);
jobject NewObject_2arg_jstr(JNIEnv *env, jclass clazz, jmethodID methodID, jstring str1, jstring str2);
jclass GetObjectClass(JNIEnv *env, jobject obj);
void CallVoidMethod_1arg_jobj(JNIEnv *env, jobject obj, jmethodID methodID, jobject arg);
jobject CallStaticObjectMethod_0arg(JNIEnv *env, jclass clazz, jmethodID methodID);
jobject CallObjectMethod_1arg_jobj(JNIEnv *env, jobject obj, jmethodID methodID, jobject arg);
jobject CallObjectMethod_1arg_jstr(JNIEnv *env, jobject obj, jmethodID methodID, jstring arg);
jobject CallStaticObjectMethod_2arg_long_int(JNIEnv *env, jclass clazz, jmethodID methodID, long arg1, int arg2);

#endif // INTENT_H_
