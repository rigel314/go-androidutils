package androidutils

import (
	"fmt"
	"log"
	"strings"
	"unsafe"

	"golang.org/x/mobile/app"
)

/*
#cgo LDFLAGS: -landroid -llog -lEGL -lGLESv2
#cgo CFLAGS: -Wall -Wextra -Werror -Wno-unused-parameter
#include "intent.h"
size_t au_GoStringLen(_GoString_ s) { return _GoStringLen(s); }
const char *au_GoStringPtr(_GoString_ s) { return _GoStringPtr(s); }
*/
import "C"

func StartActivity(intent string) error {
	return app.RunOnJVM(func(vm, jniEnv, ctx uintptr) error {
		env := (*C.JNIEnv)(unsafe.Pointer(jniEnv))
		log.Println("in JVM")

		// split intent into pkg and cls
		strs := strings.Split(intent, "/")
		if len(strs) != 2 {
			return fmt.Errorf("Bad Intent")
		}
		cls := strs[1]
		pkg := strs[0]

		// create strings
		clsjstr := C.NewStringUTF(env, nultrm(cls))
		pkgjstr := C.NewStringUTF(env, nultrm(pkg))
		intentActionJstr := C.NewStringUTF(env, nultrm("android.intent.action.MAIN"))
		intentCatagoryJstr := C.NewStringUTF(env, nultrm("android.intent.category.LAUNCHER"))

		// get componentname class
		classCompName := C.FindClass(env, nultrm("android/content/ComponentName"))
		// get componentname constructor
		methodname, sig := nultrm("<init>"), nultrm("(Ljava/lang/String;Ljava/lang/String;)V")
		CompNameConstructor := C.GetMethodID(env, classCompName, methodname, sig)
		// call constructor
		compName := C.NewObject_2arg_jstr(env, classCompName, CompNameConstructor, pkgjstr, clsjstr)

		// get intent class
		classIntent := C.FindClass(env, nultrm("android/content/Intent"))
		// get intent constructor
		methodname, sig = nultrm("<init>"), nultrm("(Ljava/lang/String;)V")
		constructor := C.GetMethodID(env, classIntent, methodname, sig)
		// call intent constructor
		intent := C.NewObject_1arg_jstr(env, classIntent, constructor, intentActionJstr)
		// get addCategory method
		methodname, sig = nultrm("addCategory"), nultrm("(Ljava/lang/String;)Landroid/content/Intent;")
		addCategory := C.GetMethodID(env, classIntent, methodname, sig)
		// call addCategory method
		C.CallObjectMethod_1arg_jobj(env, intent, addCategory, C.jobject(intentCatagoryJstr))
		// get setComponent method
		methodname, sig = nultrm("setComponent"), nultrm("(Landroid/content/ComponentName;)Landroid/content/Intent;")
		setComponent := C.GetMethodID(env, classIntent, methodname, sig)
		log.Println("after intent get setComp")
		// call setComponent method
		C.CallObjectMethod_1arg_jobj(env, intent, setComponent, compName)

		// get NativeActivity class
		classContext := C.GetObjectClass(env, C.jobject(ctx))
		// get startActivity method
		methodname, sig = nultrm("startActivity"), nultrm("(Landroid/content/Intent;)V")
		startActivity := C.GetMethodID(env, classContext, methodname, sig)
		// call startActivity method
		C.CallVoidMethod_1arg_jobj(env, C.jobject(ctx), startActivity, intent)

		return nil
	})
}

func nultrm(str string) string {
	return str + "\x00"
}
