package androidutils

import (
	"unsafe"

	"golang.org/x/mobile/app"
)

/*
#cgo LDFLAGS: -landroid -llog -lEGL -lGLESv2
#cgo CFLAGS: -Wall -Wextra -Werror -Wno-unused-parameter
#include "intent.h"
*/
import "C"

func Environment_getExternalStorageDirectory() (sdcardpath string, err error) {
	err = app.RunOnJVM(func(vm, jniEnv, ctx uintptr) error {
		env := (*C.JNIEnv)(unsafe.Pointer(jniEnv))

		// get Environment class
		classEnvironment := C.FindClass(env, nultrm("android/os/Environment"))
		// get getExternalStorageDirectory method
		methodname, sig := nultrm("getExternalStorageDirectory"), nultrm("()Ljava/io/File;")
		getExternalStorageDirectory := C.GetStaticMethodID(env, classEnvironment, methodname, sig)
		// call getExternalStorageDirectory method
		file := C.CallStaticObjectMethod_0arg(env, classEnvironment, getExternalStorageDirectory)

		// get File's class
		classFile := C.GetObjectClass(env, file)
		// get getAbsolutePath method
		methodname, sig = nultrm("getAbsolutePath"), nultrm("()Ljava/lang/String;")
		getAbsolutePath := C.GetMethodID(env, classFile, methodname, sig)
		// call getAbsolutePath method
		abspathStr := C.CallObjectMethod_0arg(env, file, getAbsolutePath)

		len := C.StringLength(env, C.jstring(abspathStr))
		sdcardpathBytes := make([]byte, len)
		C.GetStringUTF(env, (*C.uchar)(&sdcardpathBytes[0]), C.jstring(abspathStr), len)
		sdcardpath = string(sdcardpathBytes)

		return nil
	})
	return
}

func ExternalAppSpecificPath() (path string, err error) {
	err = app.RunOnJVM(func(vm, jniEnv, ctx uintptr) error {
		env := (*C.JNIEnv)(unsafe.Pointer(jniEnv))

		// get ContextCompat class
		classContext := C.GetObjectClass(env, C.jobject(ctx))
		// get getExternalFilesDir method
		methodname, sig := nultrm("getExternalFilesDir"), nultrm("(Ljava/lang/String;)Ljava/io/File;")
		getExternalFilesDir := C.GetMethodID(env, classContext, methodname, sig)
		// call getExternalFilesDir method
		file := C.CallObjectMethod_1arg_jobj(env, C.jobject(ctx), getExternalFilesDir, C.jobject(0))

		// get File's class
		classFile := C.GetObjectClass(env, file)
		// get getAbsolutePath method
		methodname, sig = nultrm("getAbsolutePath"), nultrm("()Ljava/lang/String;")
		getAbsolutePath := C.GetMethodID(env, classFile, methodname, sig)
		// call getAbsolutePath method
		abspathStr := C.CallObjectMethod_0arg(env, file, getAbsolutePath)

		len := C.StringLength(env, C.jstring(abspathStr))
		pathBytes := make([]byte, len)
		C.GetStringUTF(env, (*C.uchar)(&pathBytes[0]), C.jstring(abspathStr), len)
		path = string(pathBytes)

		return nil
	})
	return
}

// func PrimaryExternalAppSpecificPath() (path string, err error) {
// 	err = app.RunOnJVM(func(vm, jniEnv, ctx uintptr) error {
// 		env := (*C.JNIEnv)(unsafe.Pointer(jniEnv))
// 		log.Println("in JVM")

// 		// get ContextCompat class
// 		classContextCompat := C.FindClass(env, nultrm("androidx/core/content/ContextCompat"))
// 		// get getExternalFilesDirs method
// 		methodname, sig := nultrm("getExternalFilesDirs"), nultrm("()[Ljava/io/File;")
// 		getExternalFilesDirs := C.GetStaticMethodID(env, classContextCompat, methodname, sig)
// 		// call getExternalFilesDirs method
// 		files := C.jarray(C.CallStaticObjectMethod_0arg(env, classContextCompat, getExternalFilesDirs))

// 		if l := C.ArrayLength(env, files); l < 1 {
// 			return fmt.Errorf("invalid array size from ContextCompat.getExternalFilesDirs")
// 		}
// 		file := C.ArrayIndex(env, files, 0)

// 		// get File's class
// 		classFile := C.GetObjectClass(env, file)
// 		// get getAbsolutePath method
// 		methodname, sig = nultrm("getAbsolutePath"), nultrm("()Ljava/lang/String;")
// 		getAbsolutePath := C.GetMethodID(env, classFile, methodname, sig)
// 		// call getAbsolutePath method
// 		abspathStr := C.CallObjectMethod_0arg(env, file, getAbsolutePath)

// 		len := C.StringLength(env, C.jstring(abspathStr))
// 		pathBytes := make([]byte, len)
// 		C.GetStringUTF(env, (*C.uchar)(&pathBytes[0]), C.jstring(abspathStr), len)
// 		path = string(pathBytes)

// 		return nil
// 	})
// 	return
// }
