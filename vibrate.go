package androidutils

import (
	"log"
	"time"
	"unsafe"

	"golang.org/x/mobile/app"
)

/*
#cgo LDFLAGS: -landroid -llog -lEGL -lGLESv2
#cgo CFLAGS: -Wall -Wextra -Werror -Wno-unused-parameter
#include "intent.h"
*/
import "C"

func Vibrate(duration time.Duration, strength uint8) error {
	return app.RunOnJVM(func(vm, jniEnv, ctx uintptr) error {
		env := (*C.JNIEnv)(unsafe.Pointer(jniEnv))
		log.Println("in JVM")

		// get Context class
		classContextAbstract := C.FindClass(env, nultrm("android/content/Context"))
		// get VIBRATOR_SERVICE field
		fieldname, sig := nultrm("VIBRATOR_SERVICE"), nultrm("Ljava/lang/String;")
		vibSerID := C.GetStaticFieldID(env, classContextAbstract, fieldname, sig)
		vibSer := C.GetStaticObjectField(env, classContextAbstract, vibSerID)

		// get NativeActivity class
		classContext := C.GetObjectClass(env, C.jobject(ctx))
		// get getSystemService method
		methodname, sig := nultrm("getSystemService"), nultrm("(Ljava/lang/String;)Ljava/lang/Object;")
		getSystemService := C.GetMethodID(env, classContext, methodname, sig)
		// call getSystemService method
		vibe := C.CallObjectMethod_1arg_jstr(env, C.jobject(ctx), getSystemService, C.jstring(vibSer))

		// get VibrationEffect class
		classVibrationEffect := C.FindClass(env, nultrm("android/os/VibrationEffect"))
		// get createOneShot method
		methodname, sig = nultrm("createOneShot"), nultrm("(JI)Landroid/os/VibrationEffect;")
		createOneShot := C.GetStaticMethodID(env, classVibrationEffect, methodname, sig)
		// // call createOneShot method
		vibeEffect := C.CallStaticObjectMethod_2arg_long_int(env, classVibrationEffect, createOneShot, C.long(duration.Milliseconds()), C.int(strength))

		// get vibe's class
		classVibe := C.GetObjectClass(env, vibe)
		// get vibrate method
		methodname, sig = nultrm("vibrate"), nultrm("(Landroid/os/VibrationEffect;)V")
		vibrate := C.GetMethodID(env, classVibe, methodname, sig)
		// call vibrate method
		C.CallVoidMethod_1arg_jobj(env, vibe, vibrate, vibeEffect)

		return nil
	})
}
